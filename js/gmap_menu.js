(function ($) {
    $(function () {
        //reference for the map
        var map = Drupal.gmap.getMap('#gmap-nodemap-gmap0');
        // bind addmarker-handler, the marker is available as callback-argument
        map.bind('addmarker', function (m) {
            //add  your custom listener
            google.maps.event.addListener(m.marker, 'click', function () {
                 console.log(this.position.lat());
                 console.log(this.position.lng());
                var elemDiv = document.createElement('div');
                elemDiv.setAttribute("class", "content");
                elemDiv.setAttribute("id", "block-p_26");
                document.body.insertBefore(elemDiv, document.body.childNodes[1]);
                var jqXHR = $.post('./../ajax/location/titles/' + this.position.lat() + '/' + this.position.lng(), function(data) {
                    //var date = JSON.parse(data);
                    //console.log(data);
                    elemDiv.innerHTML = data;
                });
                return false;
             });
        });
    });
})(jQuery);